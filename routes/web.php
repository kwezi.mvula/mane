<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/home','HomeController@index');
Route::get('/shop','ProductController@shop');
Route::get('/shop/{slug}','ProductController@ShopCat');
Route::get('/body-products','ProductController@body');
Route::get('/face-products','ProductController@face');
Route::get('/hair-products','ProductController@hair');
Route::get('/ingredients','ProductController@ingredients');
Route::get('/details/{id}','ProductController@details');
Route::get('/about-us','AboutController@index');
Route::get('/contact-us', 'ContactController@index');
Route::resource('contactus','ContactController');
Route::post('contact', ['as'=>'contact.store','uses'=>'ContactController@store']);
Route::get('/thank-you','HomeController@thank_you');

Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::get('/cart','CartController@index');
    Route::get('/cart/addItem/{id}', 'CartController@addItem');
    Route::post('/cart/update/{id}','CartController@update');
    Route::get('/cart/remove/{id}', 'CartController@destroy');
    Route::get('/cart/update/{id}','CartController@update');
    Route::get('/checkout','CheckoutController@index');
    Route::post('/save-order','CheckoutController@store')->name('checkout.store');
    Route::get('/success','CheckoutController@success');
    Route::get('/payment-cancelled','CheckoutController@cancel');
    Route::get('/itn','CheckoutController@itn');
    Route::get('/blog','BlogController@index');
    Route::get('/post/{slug}','BlogController@post');   
});



Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
