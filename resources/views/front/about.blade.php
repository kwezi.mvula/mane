@extends('layouts.master')
@section('content')


<!-- ABOUT -->
<section data-stellar-background-ratio="0.5">
     <div class="container">
          <div class="row">
               <div class="col-md-6 col-sm-12">
                    <div class="about-info">
                         <div class="section-title wow fadeInUp" data-wow-delay="0.2s">
                              <h4>Read our story</h4>
                              <h2>About Knotty-Mane</h2>
                         </div>
                         <div class="wow fadeInUp" data-wow-delay="0.4s">
                              <p>One thing that is so rewarding about starting a handmade skincare business is the positive impact the process has on the quality of our lives and those around us that benefit from it. It compelled me to be mindful and intentional about what I do, how I choose to live and what I consume for the health of my body and mind. I have found that it helps me cultivate discipline, diligence, and accountability. Minimalism was and is at the core of my vision for this brand.</p>
                         </div>
                    </div>
               </div>

               <div class="col-md-4 col-sm-8">
                    <div class="wow fadeInUp about-image" data-wow-delay="0.6s">
                         <img src="images/about-image.jpg" style="height:430px;weight:350px" class="img-responsive" alt="">
                    </div>
               </div>      
          </div>
     </div>
</section>

<section>
     <div class="container">
          <div class="row">
               <div class="col-md-6 col-sm-6">
                    <div class="wow fadeInUp about-image" data-wow-delay="0.6s">
                         <img src="images/knotty.jpg" style="height:330px;weight:350px" class="img-responsive" alt="">
                    </div>
               </div>

               <div class="col-md-6 col-sm-12">
                    <div class="about-info">
                         <div class="section-title wow fadeInUp" data-wow-delay="0.2s">
                             <h2>Meet Charity</h2>
                         </div>

                         <div class="wow fadeInUp" data-wow-delay="0.4s">
                              <p>My name is Charity Molefe and I am a single, African, melanated mom to a beautiful girl, Tshiamo Molefe. 
                                 I started Knotty Mane for three simple reasons. Firstly, I envisioned a world where we could all return 
                                 to a natural, whole-some, and holistic approach to taking care of our skin and hair without the added fuss.
                                Being a minimalist by nature, going back to the basics has been a relief because as the saying goes, 
                                “if it ain’t broke, do not try to fix it!”. Secondly, my daughter suffered from scalp psoriasis for a long 
                                while, during which I had to cut off her beautiful hair several times in a year to allow for treatment of 
                                her inflamed scalp. In addition to that, we had to drastically change our diet. 
                              </p>
                         </div>
                    </div>
               </div> 
          
               <div class="col-md-12 col-sm-12">
                     <div class="wow fadeInUp" data-wow-delay="0.4s">
                         <p> She also leans on the side of dry/sensitive skin and as such, finding the right products for her skin was extremely 
                              frustrating and time consuming. I quickly became a “product junkie”, desperately trying to find a quick fix from 
                              every product I could lay my eyes on. I eventually decided to invest much needed time and effort in researching better 
                              and more natural ways to care for our hair and skin, as well as intentionally choosing the kinds of foods we put inside our bodies. 
                              Learning that what we apply topically is as equally important as what we consume internally was a real life changing moment for me and 
                              from that moment on, I made a conscious decision to be intentional about the products we use and the food we eat to complement our lifestyle.
                         </p>
                     </div>

                     <div class="wow fadeInUp" data-wow-delay="0.4s">
                         <p> All this coupled with my love for all things beauty, I began to enjoy mixing herbal concoctions and whipping up my own hair and body butters, 
                            handing it to friends and family to try et Voila!, Knotty Mane was born. Understanding how products affect 
                            my skin and health gives me peace of mind, and enough time to focus on living each day focusing on other 
                            things that matter most to me, like spending time with my loved ones, minus ashy skin and crusty feeling hair. 
                         </p>
                   </div>
               </div>
          </div>
     </div>
</section>


@endsection