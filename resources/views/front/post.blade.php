@extends('layouts.master')
@section('content')
<!-- ABOUT -->
<section data-stellar-background-ratio="0.5">
     <div class="container">
          <div class="row">
               <div class="col-md-6 col-sm-12">
                    <div class="about-info">
                         <div class="section-title wow fadeInUp" data-wow-delay="0.2s">
                              <h2>{{$post->title}}</h2>
                              <h4>{{$post->created_at}}</h4>
                         </div>
                         <div class="wow fadeInUp" data-wow-delay="0.4s">
                              <p>{{$post->excerpt}}</p>
                         </div>
                    </div>
               </div>

               <div class="col-md-4 col-sm-8">
                    <div class="wow fadeInUp about-image" data-wow-delay="0.6s">
                         <img src="/storage/{{$post->image}}"  class="img-responsive" alt="">
                    </div>
               </div> 
               
               <div class="col-md-12 col-sm-12" style="padding-top:50px">
                <div class="about-info">
                    <div class="wow fadeInUp" data-wow-delay="0.4s">
                        <p>{!! html_entity_decode($post->body) !!}</p>
                    </div>

                </div>
           </div>
          </div>
     </div>
</section>

@include('includes.latest')
@endsection