@extends('layouts.master')
@section('content')



</section>
     <!-- ABOUT -->
     <section id="about" data-stellar-background-ratio="0.5">
        <div class="container">
             <div class="row">

                  <div class="col-md-6 col-sm-12">
                       <div class="about-info">
                            <div class="section-title wow fadeInUp" data-wow-delay="0.2s">
                                 <h4>Read our story</h4>
                                 <h2>Ingredients</h2>
                            </div>

                            <div class="wow fadeInUp" data-wow-delay="0.4s">
                                 <p><strong>ALOE VERA</strong> - Aloe barbadensis - Aloe vera is an excellent skin-soothing and moisturizing ingredient. It has been found to boost healing, collagen production, and hyaluronic acid production in the skin. It has anti-inflammatory, anti-viral, and antiseptic effects. Aloe vera contains vitamins A, C, and E.</p>
                                 <p><strong>APRICOT KERNEL OIL</strong>- Prunus armeniaca - A non-greasy, easily absorbing oil crushed from the apricot kernel. Ideal for dry, mature, sensitive skin. It is rich in vitamin A.</p>
                                 <p><strong>AVOCADO OIL</strong> - Persea Gratissima Oil - Avocado oil is a beautiful, rich emollient that is recommended for dry and sensitive skin.</p>
                            </div>
                       </div>
                  </div>

                  <div class="col-md-6 col-sm-12">
                       <div class="wow fadeInUp about-image" data-wow-delay="0.6s">
                            <img src="{{URL::asset('images/about-image.jpg')}}" class="img-responsive" alt="">
                       </div>
                  </div>

                  <div class="col-md-12 col-sm-12">
                    <div class="about-info">
                         <div class="wow fadeInUp" data-wow-delay="0.4s">
                              <p><strong>ARGAN (ORGANIC) OIL</strong> - Argania spinosa -. A super versatile oil for face, body, hair, eyelashes and nails. Boosts shine and condition of hair and tames frizz, Optimum balance of fatty essential acids, Particularly beneficial for mature skin, Rich in Oleic acid (Omega 9), Linoleic acid (Omega 6) and Vitamin E, Exceptional moisturiser.</p>
                              <p><strong>BAOBAB OIL</strong> - Adansonia digitata - Native to Africa, Baobab oil has a good balance of fatty acids, including Oleic, Linoleic and Alpha Linolenic. It is suitable for all skin types particularly those with dry skin. Offers lightweight, intensive moisturisation. Non-greasy and fast absorbing. Ideal for hair when it lacks lustre and in need of reviving.</p>
                              <p><strong>BLACK SEED</strong> - Nigella sativa - Also known as the black cumin oil, blessed seed, kalonji and Egyptian black cumin, Black Seed oil is cold pressed. Rich in vitamins, it is a remarkable oil for skin ailments and hair treatment. </p>
                              <p><strong>BTMS-50</strong> - Behentrimonium Methosulfate (and) Cetyl Alcohol (and) Butylene Glycol - BTMS-50 is a cationic (positively charged) emulsifying wax. Because it’s cationic it is also “conditioning”—it adsorbs (creates a very fine coating) on skin and hair, giving an amazing finish that is unique to cationic ingredients. It contains 50% of the active ingredients, Behentrimonium Methosulfate.</p>
                              <p><strong>CACAY OIL</strong> - Caryodendron Orinocense - A luxurious oil brought to you direct from the Amazon extracted from the nuts of the Cacay Tree. Packed with essential fatty acid Linoleic Acid (Omega 6), vitamins E and A. Particularly useful for mature and/or very dry skin. Intensively moisturising and nourishing. Helps hair retain moisture and conditions hair.</p>
                              <p><strong>CALENDULA</strong> - Calendula officinalis - Calendula is an infused oil. The calendula flowers contain a variety of compounds which are believed tosupport wound healing. Herbalists believe calendula is very versatile, speeding the healing of wounds, making the skin suppler by increasing blood supply to it, and soothing pain when used topically.</p>
                              <p><strong>CARROT OIL</strong> - Daucus carota - Carrot oil, cold pressed, is rich in beta carotene, vitamins A, B, C, D, E and F, as well as essential fatty acids, it is also a strong free radical blocker. This oil is suitable for all skin types; it balances the moisture in the skin, restores elasticity and smoothes tired, dried out skin and is good for general skin health.</p>
                              <p><strong>CASTOR OIL</strong> - Ricinus communis - Rich, nourishing and packed with skin loving essential fatty acids, this multifunctional oil ticks all the boxes. Castor can be used for eyelashes, eyebrows, nails, cuticles, hair and skin, helping to nourish, revive and revitalise. Packed with essential fatty acids, thick, honey like consistency. Hydrating and nourishing. Amazing for hair and skin. Strengthens, nourishes, and conditions hair.</p>
                              <p><strong>COCOA BUTTER</strong> - Theobroma Cacao - Rich, solid butter full of naturally beneficial nutrients. A rich source of fatty acids and antioxidants. Deliciously, chocolatey aroma. Intensively moisturising and protects from water loss.</p>
                              <p><strong>COCONUT OIL</strong> - Cocos Nucifera - Fractioned coconut is useful in formulations for dry, itchy, sensitive skin. It is light and non-greasy, will not clog pores, and it absorbs readily into the skin. It contains Lauric Acid, Vitamin A, Vitamin B1, Vitamin B2, Vitamin B6 and Vitamin E as well as phosphorus, calcium, iron, manganese and magnesium. Herbal hair infusions are often infused in Coconut Oil.</p>
                              <p><strong>EMULSIFYING WAX NF</strong> - this wax is a plant based emollient and emulsifier.</p>
                              <p><strong>ESSENTIAL OILS</strong> - 100% pure, undiluted, concentrated aromatic oils distilled from herbs, spices, fruits, flowers. </p>
                              <p><strong>EVENING PRIMROSE OIL</strong> - Oenothera Biennes - Evening Primrose is a perfect skin care oil with a delicate, slightly sweet aroma. It is moisturising, softening, and soothing to dry and irritated skin. Excellent oil for face and body massage blends, particularly for dry, scaly, devitalized and prematurely aging skin. It is purported to reduce hyperactivity in babies and young children when rubbed onto their skin.</p>
                              <p><strong>FENUGREEK</strong> - it gets its name from Greek language and means “Greek Hay. Fenugreeks seeds and leaves are rich in other nutrients like Vitamin B1, B2, B3, B6, Folic acid and Vitamin C. It also contains various minerals like Potassium, Copper, Selenium, Zinc etc. Vitamin K is present in fenugreek leaves. The health benefits of fenugreek can also be attributed to the presence of soluble fibre present in fenugreek seeds, along with saponins, essential amino acids like lysine and l-tryptophan, mucilage, tannins, pectins and hemi-cellulose. Fenugreek seeds contain a substance called as nicotinic acid, along with lecithin, both of which are good for hair growth. It keeps the scalp cool and helps in reducing the occurrence of dandruff. It acts as a hair conditioner, reduces hair loss, and strengthens the hair follicles. All these benefits of fenugreek can be obtained by topical application of fenugreek through masks and packs, as well as incorporating it in the diet.</p>
                              <p><strong>GRAPESEED OIL</strong> - Vitis vinifera - This oil is a non- greasy with a fine texture and satiny finish it is rich in linoleic acid, important for the skin and the cell membranes, it is purported to have regenerative and restructuring virtues and has great skin moisturising properties. The antioxidants, vitamins and other nutrients present in the oil makes it an attractive remedy for a variety of skin disorders.</p>
                              <p><strong>HEMP SEED OIL</strong> - Cannabis sativa - Moisturising soothing nutritive oil pressed from hemp seeds. Good for dry skin and itchy scalp. Lightweight and fast absorbing. Non-comedogenic. Rich in Omega 3 and 6 fatty acids.  Balancing and hydrating. Amazing as a massage oil.</p>
                              <p><strong>HERBS</strong> - We use various herbs for their natural properties. Most of the herbs we use also contain anti-inflammatory and strengthening properties.</p>
                              <p><strong>JOJOBA OIL</strong> - Simmondsia chinensis - Jojoba is not an oil but a liquid wax composed of wax esters; because of this it is an extremely stable, substance and does not easily deteriorate or turn rancid. It contains Vitamin A, Vitamin B1, Vitamin B2, Vitamin B6; Vitamin E. It also has the minerals silicon, chromium, copper and zinc and a very high percentage of iodine, giving Jojoba oil a great power to heal.</p>
                              <p><strong>MACADAMIA NUT OIL </strong> - Macadamia ternifolia - Rich in Oleic Acid (Omega 9). A super lightweight hydrator, helping to seal in moisture. Excellent for those with dry skin. Adds shine and calms frizz when applied to damp hair before styling.</p>
                              <p><strong>MANGO BUTTER</strong> - Mangifera indica - obtained from the fruit seed of the mango tree, mango butter melts on skin contact to moisturise extremely dry skin. A wonderful emollient, it adds a rich creamy feel to lotions and creams. Ideal for afro hair. </p>
                              <p><strong>NEEM OIL </strong> - Azadirachta indica - An evergreen plant with properties to help treat fungal infections. A wonder oil for your hair and scalp. Seals in moisture and creates a protective layer on the skin. </p>
                              <p><strong>OLIVE OIL</strong> - Olea europea - Contains around 70% Oleic Acid (Omega 9). High in natural antioxidants and Palmitoleic Acid, good for mature skin. Super moisturising, ideal for those requiring intense moisturisation. Ultra-conditioning for hair and helps to retain moisture.</p>
                              <p><strong>SEA BUCKTHORN OIL </strong> - Hippophae Rhamnoides - Rejuvenating, hydrating and soothing. Beautiful bright orange oil. Perfect hydration boost when added to your moisturiser. Breathes life into dull, lifeless hair.</p>
                              <p><strong>SHEA BUTTER</strong> - Butyrospermum Parkii - Beautifully soft and nourishing certified organic butter extracted from the Shea Nut. Melts into your skin. Packed with skin loving nutrients. Rich in antioxidants. Hydrates and protects dry skin.</p>
                              <p><strong>SUNFLOWER OIL </strong> - Helianthus annus - Sunflower oil can be used as a main oil or in a blend it has a pleasant aroma and is easily applied and absorbed it can help to moisturize, regenerate, soften and condition the skin. An oil wealthy in Oleic acid with high amounts of Vitamins A, B1, B2, B6, D, and E, it is rich in protein, linoleic acid, minerals calcium, zinc, potassium, iron, and phosphorus.</p>
                              <p><strong>SWEET ALMOND OIL </strong> - Prunus Amygdalu Dulcis - Almond oil is soft, soothing, and nourishing to the skin; is non greasy, spreads easily and is good for all skin types. This oil helps protect the surface of the skin and acts as an emollient, skin soother and softener, conditions the skin and promotes a clear young-looking complexion. Almond oil can also help relieve muscular aches and pains.</p>
                              <p><strong>VEGETABLE GLYCERIN </strong> - A natural humectant. Draws water from its surroundings. Excellent moisturising ingredient in homemade skincare products. Non-comedogenic, so it will not block your pores. Can help your skin maintain a natural balance.</p>
                              <p><strong>VITAMIN E OIL </strong> - A natural antioxidant that helps fights free radicals in the skin. Super moisturising and nourishing. Can help rejuvenate and hydrate dry skin. Suitable for all skin types, especially mature skin. Can be used directly on the skin as all over moisturiser</p>
                         </div>
                    </div>
               </div>
                  
             </div>
        </div>
   </section>

@endsection
