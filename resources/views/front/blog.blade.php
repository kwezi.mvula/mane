@extends('layouts.master')
@section('content')
<section id="about" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="about-info">
                     <div class="section-title wow fadeInUp" data-wow-delay="0.2s">
                        <h2>Recent News</h2>
                        <h4>Blog Posts</h4>
                     </div>
                     <div class="col-md-5 col-sm-6">
                        <!-- MENU THUMB -->
                        <div class="menu-thumb">
                             <a href="/storage/{{$post->image}}" class="image-popup" title="American Breakfast">
                                  <img src="/storage/{{$post->image}}" class="img-responsive" alt="">

                                  <div class="menu-info">
                                       <div class="menu-item">
                                            <h3>{{ $post->title }}</h3>
                                            <p>{{ $post->excerpt }}</p>
                                            <p>{{ $post->created_at }}</p>
                                       </div>
                                       <div class="menu-price">
                                            <ul class="social-icon">
                                                <li><a style="font-size: 25px" href="{{ URL::to('post/'.$post->slug) }}" class="fa fa-info-circle"></a></li>
                                            </ul>
                                       </div>
                                  </div>
                             </a>
                        </div>
                    </div>

                    @foreach($new as $post)
                        <div class="col-md-3 col-sm-5">
                            <div class="team-thumb wow fadeInUp" data-wow-delay="0.2s">
                                <img src="/storage/{{$post->image}}" class="img-responsive" alt="">
                                <div class="team-hover">
                                    <div class="team-item">
                                        <ul class="social-icon">
                                            <li><a style="font-size: 25px" href="{{ URL::to('post/'.$post->slug) }}" class="fa fa-info-circle"></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="team-info">
                                <h3>{{ $post->title }}</h3>
                                <h4>{{ $post->excerpt }}</h4> 
                                <p>{{ $post->created_at }}</p>
                            </div>
                        </div>   
                    @endforeach        
                </div>
            </div>
        </div>
    </div>
</section>

@include('includes.latest')
@include('includes.recent')
@include('includes.popular')
@endsection