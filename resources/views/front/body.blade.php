@extends('layouts.master')
@section('content')


     <!-- ABOUT -->
     <section id="about" data-stellar-background-ratio="0.5">
        <div class="container">
             <div class="row">

                  <div class="col-md-6 col-sm-12">
                       <div class="about-info">
                            <div class="section-title wow fadeInUp" data-wow-delay="0.2s">
                                 <h4>Read our story</h4>
                                 <h2>Body Products</h2>
                            </div>

                            <div class="wow fadeInUp" data-wow-delay="0.4s">
                                <h2>Hand Rescue Cream</h2>
                                <h3>Description</h3>
                                 <p>Our hand rescue cream provides intensive moisture and hydration to dry skin and is perfect for cuticles. Enriched with natural ingredients containing Anti-bacterial & anti-inflammatory properties.</p>
                                 <p><strong>How to use: </strong>Apply small amount to hands and cuticles as needed.</p>
                                 <p><strong>Ingredients: </strong> Distilled Water, Aloe Vera gel, Shea Butter, Cetearyl Alcohol, Avocado oil, Castor Seed Oil, Carrot seed Oil, Hemp Seed Oil, Neem Oil, Vitamin E, Essential oils of Lavender and Patchouli.</p>
                                 <p><strong>Storage:</strong> Store in a cool place, away from direct heat and sunlight.</p>
                            </div>
                       </div>
                  </div>

                  <div class="col-md-6 col-sm-12">
                       <div class="wow fadeInUp about-image" data-wow-delay="0.6s">
                            <img src="{{URL::asset('images/about-image.jpg')}}" class="img-responsive" alt="">
                       </div>
                  </div>

                  <div class="col-md-12 col-sm-12">
                    <div class="about-info">
                        <div class="wow fadeInUp" data-wow-delay="0.4s">
                            <h2>Sensual Massage Oil</h2>
                            <h3>Description</h3>
                             <p>Our yummy, sensual, aphrodisiac massage oil is ideal for hot oil massages. It is intended to warm you up and relax your body.It promotes relaxed, uplifting euphoric feelings, while warming and softens muscles. Turn on some calming music, light up candles and enjoy! Simply heat the bottle in a jar of hot water and use.</p>
                             <p><strong>Ingredients: </strong>Grapessed Oil, Essential oils of Sandalwood, Orange, Patchouli, Ylang Ylang and Ginger.</p>
                             <p><strong>Disclaimer:</strong> Orange may cause photosensitivity and sunburn. Do not use before sun exposure.</p>
                        </div>

                        <div class="wow fadeInUp" data-wow-delay="0.4s">
                            <h2>Whipped Shea Body Butter (Fragrance Free)</h2>
                            <h3>Description</h3>
                             <p>Our luxurious body butter is perfect for dry and sensitive skin. Easy melts into your skin leaving a smooth, soft coating. It is fragrance free, safe for use on both adults and children. It is enriched with Vitamin E, leaving your skin rejuvenated and hydrated.</p>
                             <p><strong>Ingredients: </strong>Shea Butter, Grapeseed Oil, Macadamia Nut Oil, Sea Buckthorn Oil, Jojoba Oil, Apricot Kernel Oil, Vitamin E.</p>
                             <p><strong>Storage:</strong> Store in a cool place, away from direct heat and sunlight.</p>
                             <p>This product is 100% natural and handmade in small batches. No parabens, no minerals, no nasty chemicals.</p>
                        </div>
                    </div>
               </div>
                  
             </div>
        </div>
   </section>

@endsection
