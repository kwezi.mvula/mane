@extends('layouts.master')
@section('content')



</section>
     <!-- ABOUT -->
     <section id="about" data-stellar-background-ratio="0.5">
        <div class="container">
             <div class="row">

                  <div class="col-md-6 col-sm-12">
                       <div class="about-info">
                            <div class="section-title wow fadeInUp" data-wow-delay="0.2s">
                                 <h4>Read our story</h4>
                                 <h2>Facial Products</h2>
                            </div>

                            <div class="wow fadeInUp" data-wow-delay="0.4s">
                                <h2>Herbal Acne Face Salve</h2>
                                <h3>Description</h3>
                                 <p>Intended to be used on acne prone skin. To soothe and heal dry, irritated skin. Tones and tightens skin, helps maintain firmness and elasticity. encourages cell regeneration, stimulates circulation.</p>
                                 <p><strong>Ingredients: </strong>Beeswax, Hazelnut oil, Hempseed Oil, Apricot Kernel, Tea tree essential oil</p>
                                 <p><strong>Storage:</strong>Store in a cool place, away from direct heat and sunlight.</p>
                                 <p>This product is 100% natural and handmade in small batches. No parabens, no minerals, no nasty chemicals.</p>
                            </div>
                       </div>
                  </div>

                  <div class="col-md-6 col-sm-12">
                       <div class="wow fadeInUp about-image" data-wow-delay="0.6s">
                            <img src="{{URL::asset('images/about-image.jpg')}}" class="img-responsive" alt="">
                       </div>
                  </div>

                  <div class="col-md-12 col-sm-12">
                    <div class="about-info">
                        <div class="wow fadeInUp" data-wow-delay="0.4s">
                            <h2>Beard Oil</h2>
                            <h3>Description</h3>
                             <p>Our Beard oil is specially formulated to help soothe irritation and hydrate the skin. Apply a few drops to fingertips and gently massage into skin and beard.</p>
                             <p><strong>Ingredients: </strong>Vitis vinifera (Grapeseed) Oil, Calendula officinalis, Daucus carota (Carrot) Oil, Cannabis sativa (Hemp) Oil, Ricinus Communis (Castor) Oil, Simmondsia Chinensis (Jojoba) Oil, Blend of Essential Oils, Vitamin E.</p>
                             <p><strong>Storage:</strong> Store in a cool place, away from direct heat and sunlight.</p>
                             <p>This product is 100% natural and handmade in small batches. No parabens, no minerals, no nasty chemicals.</p>
                        </div>

                        <div class="wow fadeInUp" data-wow-delay="0.4s">
                            <h2>Under eye Serum</h2>
                            <h3>Description</h3>
                             <p>To lighten stubborn, dark circles under eyes. Apply 1 drop of the oil to your finger and gently smoothe the oil beneath your eye and onto the area just beneath the brow bone. Do not apply oil directly to your eyelids.</p>
                             <p><strong>Ingredients: </strong>Distilled Water, Aloe vera gel, Grapeseed Oil, Carrot oil, Saffron Oil, Turmeric powder, Cetearyl Alcohol, preservative.</p>
                             <p><strong>Saffron</strong> - Has great skin brightening and lightening effects. Reduces pigmentation and brightens skin tone.</p>
                             <p><strong>Turmeric</strong>- Has skin lightening and brightening effects.</p>
                             <p><strong>Aloe gel</strong>- Hydrates the under eyes and prevents early wrinkles.</p>
                             <p>Grapeseed oil is light in texture and is easily absorbed by the skin. Making it perfect for normal to oily skin. Grape seed oil has mildly astringent qualities, which help to tighten and tone the skin, making it a good cosmetic ingredient for anti-aging products. Grapeseed oil is known to contain powerful antioxidants. It is noted to be especially effective for repair of the skin around the eyes and works great for acne. </p>
                        </div>

                        <div class="wow fadeInUp" data-wow-delay="0.4s">
                            <h2>Nourishing Facial Oil</h2>
                            <h3>Description</h3>
                             <p>Our unscented facial oil is rich in potent skin beneficial oils that target specific concerns and help lighten fine lines and prevent early wrinkles. It is safe for all skin types including dry and damaged skin. It nourishes the skin and increases elasticity, while reducing the appearance of pre-mature aging, repairing dull looking skin. An excellent moisturiser for soft, youthful skin.</p>
                             <p><strong>Ingredients: </strong>Argan Oil, Carrot Seed Oil, Evening Primrose Oil, Safflower Oil, Vitamin E</p>
                             <p><strong>Directions:</strong> Apply 3-5 drops into hands and gently massage face and neck area</p>
                             <p>This product is 100% natural and handmade in small batches. No parabens, no minerals, no nasty chemicals.</p>
                        </div>

                        <div class="wow fadeInUp" data-wow-delay="0.4s">
                            <h2>Acne Facial Oil</h2>
                            <h3>Description</h3>
                             <p>Our acne facial oil is a concentrated blend of potent oils that target specific skin concerns. Facial oils also keep skin well-moisturized and healthy. Use 2-3 drops for the entire face before applying a moisturizer.</p>
                             <p><strong>Jojoba oil</strong> - Has anti-acne, skin moisturizing, softening and scar lightening properties.</p>
                             <p><strong>Vitamin E oil</strong> - Moisturizes skin, adds glow, softens skin</p>
                             <p><strong>Hemp seed oil</strong> - A green oil that in cold pressed from hemp seeds. Very high in omega 6 and omega 3 fatty acids, antioxidants, vitamins and minerals. Moisturizes the skin but is also used to treat acne (including cystic acne), reduce the appearance of scars, reduce redness, and promote healing. It is also used for skin conditions such as eczema and psoriasis. Does not clog pores.</p>
                        </div>
                    </div>
               </div>
                  
             </div>
        </div>
   </section>

@endsection
