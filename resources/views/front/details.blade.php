@extends('layouts.master')
@section('content') 


<section id="team" data-stellar-background-ratio="0.5">
    <section class="breadcrumb-section set-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>Details</h2>
                        <div class="breadcrumb__option">
                            <a href="{{ asset('/')}}">Home</a>
                            <span>Details</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->
</section>
<section id="about" data-stellar-background-ratio="0.5">
    <div class="container">
         <div class="row">
            <div class="col-md-8 col-sm-12">
                   <div class="about-info">
                        <div class="section-title wow fadeInUp" data-wow-delay="0.2s">
                            <h2>{{$product->pro_name}}</h2>
                            <h3>${{$product->pro_price}}</h3>
                        </div>

                        <div class="wow fadeInUp" data-wow-delay="0.4s">
                            <p>{{$product->pro_info}}</p>
                            <p></p>
                        </div>
                   </div>
              </div>
              <div class="col-md-3 col-sm-2">
                <div class="team-thumb wow fadeInUp" data-wow-delay="0.2s">
                    <img src="/storage/{{$product->image}}" class="img-responsive" alt="">
                        <div class="team-hover">
                            <div class="team-item">
                                    <ul class="social-icon">
                                        <li><a href="{{ URL::to('cart/addItem/'.$product->id) }}" class="fa fa-shopping-cart"></a></li>
                                    </ul>
                            </div>
                        </div>
                </div>
              </div>  
         </div>
    </div>
</section>

@include('includes.recent') 

@include('includes.popular')

@endsection