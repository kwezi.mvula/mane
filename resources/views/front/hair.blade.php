@extends('layouts.master')
@section('content')



</section>
     <!-- ABOUT -->
     <section id="about" data-stellar-background-ratio="0.5">
        <div class="container">
             <div class="row">

                  <div class="col-md-6 col-sm-12">
                       <div class="about-info">
                            <div class="section-title wow fadeInUp" data-wow-delay="0.2s">
                                 <h4>Read our story</h4>
                                 <h2>Hair Products</h2>
                            </div>

                            <div class="wow fadeInUp" data-wow-delay="0.4s">
                                <h2>Whipped Shea Hair Butter</h2>
                                <h3>Description</h3>
                                 <p>Our luxurious Shea hair butter is whipped to a creamy consistency and melts upon contact. It is enriched with a blend of ayurvedic herbs for added strength and shine. Use it to coat your strands and seal in moisture and prevent excessive frizz and damage/breakage. Best applied on slightly damp hair.</p>
                                 <p><strong>Ingredients: </strong>Unrefined raw Shea Butter, Jojoba Oil, Olive Oil infused w/Ayurvedic herbs, Neem Oil, Vitamin E, Blend of Essential oils.</p>
                                 <p><strong>Storage:</strong>Store in a cool dry place, away from direct sunlight and heat. To prevent mould, do not use wet fingers to scoop product.</p>
                            </div>
                       </div>
                  </div>

                  <div class="col-md-6 col-sm-12">
                       <div class="wow fadeInUp about-image" data-wow-delay="0.6s">
                            <img src="{{URL::asset('images/about-image.jpg')}}" class="img-responsive" alt="">
                       </div>
                  </div>

                  <div class="col-md-12 col-sm-12">
                    <div class="about-info">
                        <div class="wow fadeInUp" data-wow-delay="0.4s">
                            <p>This product is 100% natural and handmade in small batches. Consistency may vary from batch to batch. No parabens, no minerals, no nasty chemicals.</p>
                            <p>Our labels are not waterproof and as such, print may fade overtime. </p>
                            <p><strong>DISCLAIMER:</strong> Product may melt in extremely hot temperatures. Let it cool at room temperature and enjoy</p><br>
                            <h2>Ayurvedic Hair Growth Oil</h2>
                            <h3>Description</h3>
                             <p>Our Ayurvedic hair growth oil is made up of carefully selected herbs and fruits that have strengthening, disinfecting, nutritional and nourishing properties aimed at helping to soothe an irritated scalp, combat dryness and reduce dandruff. Our luxurious oil is stored in dark bottles to preserve the potency and quality of the herbs so that you can enjoy their benefits for longer. Use it for hot oil treatments and scalp massages. It does not clog your pores.</p>
                             <p><strong>Ingredients: </strong>Olive Oil, Fenugreek, Neem Oil, Sunflower Oil, Tulsi, Ginger extract, Moringa Oil, Burdock Root, Blend of Essential Oils.</p>
                             <p><strong>Storage:</strong>Store in a cool dry place, away from direct sunlight and heat.</p>
                             <p>This product is 100% natural and handmade in small batches. No parabens, no minerals, no nasty chemicals.</p>
                             <p>Our labels are not waterproof and as such, print may fade overtime. Production may take up to 7days.</p>
                        </div>
                    </div>
               </div>
                  
             </div>
        </div>
   </section>

@endsection
