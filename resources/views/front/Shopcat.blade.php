@extends('layouts.master')
@section('content')



<section id="about" data-stellar-background-ratio="0.5">
    <div class="container">
         <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="about-info">
                     <div class="section-title wow fadeInUp" data-wow-delay="0.2s">
                        <h2>KnottyMane Shop</h2>
                          <h4>{{ $category->name }}</h4>
                     </div>
                    <div class="col-md-4">
                        <div class="hero__categories">
                            <div class="hero__categories__all">
                                <i class="fa fa-bars"></i>
                                <span>All Categories</span>
                            </div>
                            <ul>
                                @forelse ($categories as $category)
                                <li><a href="/shop/{{ $category->slug }}">{{ $category->name }}</a></li>
                                @empty
                                    
                                @endforelse
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-8">
                        @forelse ($products as $product)
                            <div class="col-md-3 col-sm-2">
                                <div class="team-thumb wow fadeInUp" data-wow-delay="0.2s">
                                    <img src="/storage/{{$product->image}}" class="img-responsive" alt="">
                                    <div class="team-hover">
                                        <div class="team-item">
                                            <ul class="social-icon">
                                                <li><a href="{{ URL::to('cart/addItem/'.$product->id) }}" class="fa fa-shopping-cart"></a></li>
                                                <li><a href="{{ URL::to('details/'.$product->id) }}" class="fa fa-info-circle"></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="team-info">
                                    <h4>{{ $product->pro_name }}</h4>
                                    <p>${{ $product->pro_price }}</p>
                                </div>
                            </div>     
                        @empty
                            <div class="col-md-3 col-sm-4">
                                <div class="team-info">
                                <h2>Products currently unavailable</h2>
                                </div>
                            </div>      
                        @endforelse
                    </div> 
                </div>
            </div> 
         </div>
    </div>
</section>

@include('includes.recent')
@include('includes.popular')

@endsection