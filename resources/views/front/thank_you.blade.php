@extends('layouts.master')
@section('content')
<section id="about" data-stellar-background-ratio="0.5">
    <div class="container">
         <div class="row">

              <div class="col-md-6 col-sm-12">
                   <div class="about-info">
                        <div class="section-title wow fadeInUp" data-wow-delay="0.2s">
                             <h2>Thank You</h2>
                        </div>

                        <div class="wow fadeInUp" data-wow-delay="0.4s">
                             <p>We have received your message and would like to thank you for contacting us. If your inquiry is urgent, please use the telephone number listed below to talk to one of our staff members. Otherwise, we will reply by email as soon as possible.</p>
                             <p>Talk to you soon.</p>
                             <p>Knotty Mane</p>
                        </div>
                   </div>
              </div>

              <div class="col-md-6 col-sm-12">
                   <div class="wow fadeInUp about-image" data-wow-delay="0.6s">
                        <img src="images/about-image.jpg" class="img-responsive" alt="">
                   </div>
                   
              </div>
              
         </div>
    </div>
</section>
@include('includes.recent')
@include('includes.popular')
@endsection