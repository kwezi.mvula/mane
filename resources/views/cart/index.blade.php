@extends('layouts.master')
@section('content')

<section id="about" data-stellar-background-ratio="0.5">
    <div class="container">
         <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="about-info">
                     <div class="section-title wow fadeInUp" data-wow-delay="0.2s">
                        <h2>My ShoppingCart</h2>
                        <h4>All Products</h4>
                     </div>
            <div class="col-lg-12">
                <div class="shoping__cart__table">
                    <table>
                        <thead>
                            <tr>
                                <th class="shoping__product">Products</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Total</th>
                                <th colspan="2"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($cartItems as $cartItem)
                            <tr>
                                <td class="shoping__cart__item">
                                    <img src="/storage/{{$cartItem->image}}" alt="">
                                    <h5>{{ $cartItem->name }}</h5>
                                </td>
                                <td class="shoping__cart__price">
                                    ${{ $cartItem->price }}
                                </td>
                                <form action="{{ url('cart/update/'.$cartItem->rowId) }}" method="POST"  enctype="multipart/form-data">
                                    @csrf
                                    <td class="shoping__cart__quantity">
                                        <div class="quantity">
                                            <div class="pro-qty">
                                                <input type="number" size="2" value="{{$cartItem->qty}}" name="qty" id="qty"
                                            autocomplete="off"   MIN="1" MAX="100">
                                            </div>
                                        </div>
                                    </td>
                                    <td class="shoping__cart__total">
                                        {{ Cart::subtotal() }}
                                    </td>
                                    <td class="shoping__cart__item__close">
                                        <button type="submit" class="btn btn-success"><i class="fa fa-refresh"></i></button>
                                    </td> 
                                    <td class="shoping__cart__item__close">
                                        <a href="{{ URL::to('cart/remove/'.$cartItem->rowId) }}"class="btn btn-danger" onclick="return confirm('Are you sure you?')"><i class="fa fa-trash"></i></a>
                                    </td>
                                </form>             
                            </tr>
                            @empty
                            <tr>
                                <td>Cart is empty</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="shoping__cart__btns">
                    <a href="{{ asset('/shop') }}" class="primary-btn cart-btn">CONTINUE SHOPPING</a>
                </div>
            </div>
            <!--<div class="col-lg-6">
                <div class="shoping__continue">
                    <div class="shoping__discount">
                        <h5>Discount Codes</h5>
                        <form action="#">
                            <input type="text" placeholder="Enter your coupon code">
                            <button type="submit" class="site-btn">APPLY COUPON</button>
                        </form>
                    </div>
                </div>
            </div>-->
            <div class="col-lg-6">
                <div class="shoping__checkout">
                    <h5>Cart Total</h5>
                    <ul>
                        <li>Subtotal <span>${{ Cart::subtotal() }}</span></li>
                        <li>Tax <span>${{ Cart::tax() }}</span></li>
                        <li>Total <span>${{ Cart::total() }}</span></li>
                    </ul>
                    <a href="{{ asset('/checkout') }}" class="btn btn-success">PROCEED TO CHECKOUT</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Shoping Cart Section End -->

@endsection
