@extends('layouts.master')
@section('content')

<!-- Checkout Section Begin -->
<section id="about" data-stellar-background-ratio="0.5">
    <div class="container">
         <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="about-info">
                     <div class="section-title wow fadeInUp" data-wow-delay="0.2s">
                        <h2>Checkout</h2>
                        <h4>Billing Details</h4>
                     </div>
                    <div class="checkout__form">
                        <form action="{{ route('checkout.store') }}" method="POST">
                            @csrf
                            <div class="row">
                                <div class="col-lg-8 col-md-6">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="cf-name" name="name" placeholder="{{ auth()->user()->name }}" value="{{ auth()->user()->name }}">
                                        </div>    
                                    </div>
                                    
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="cf-surname" name="surname" placeholder="Last name" required autocomplete="surname">
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <input type="email" class="form-control" id="cf-email" name="email" placeholder="{{ auth()->user()->email }}" value="{{ auth()->user()->email }}">
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <input type="number" class="form-control" id="cf-phone" name="phone" placeholder="Phone Number" required autocomplete="phone">
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <textarea class="form-control" rows="6" id="cf-address" name="address" placeholder="Street Address" required autocomplete="address"></textarea>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="cf-city" name="city" placeholder="Town/City" required autocomplete="city">
                                        </div>
                                    </div>
                                
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="cf-country" name="country" placeholder="Country" required autocomplete="country">
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="cf-area" name="area_code" placeholder="Area Code" required autocomplete="area_code">
                                        </div>
                                    </div>

                                    @foreach($Items as $item)  
                                        <input type="hidden" name="item_name" value="{{$item->name}}">
                                        <input type="hidden" name="quantity" value="{{$item->qty}}">
                                        <input type="hidden" name="product_id" value="{{ $item->id }}" >
                                        <input type="hidden" name="total" value="{{{Cart::total()}}}">
                                    @endforeach  

                                    <input type="hidden" name="order_code" value="{{$randomOrderNumber}}">
                                    
                                    <div class="col-lg-8 col-md-8">
                                        <div class="form-group">
                                            <center><button type="submit" class="btn btn-success" id="cf-submit" name="submit">Proceed To Payment</button></center>
                                            <div id="paypal-button-container"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="checkout__order">
                                        <h4>Your Order</h4>
                                        <div class="checkout__order__products">Products <span>Price</span></div>
                                        @forelse($Items as $item)
                                        <ul>
                                            <li>{{ $item->name }} <span>${{ $item->price }}</span></li>
                                        </ul>    
                                        @empty
                                        <ul>
                                            <li>You have no Items on cart</li>
                                        </ul>   
                                        @endforelse
                                        
                                        <div class="checkout__order__subtotal">Subtotal <span>${{ Cart::subtotal() }}</span></div>
                                        <div class="checkout__order__total">Tax <span>${{ Cart::tax() }}</span></div>
                                        <div class="checkout__order__total">Total <span>${{ Cart::total() }}</span></div>
                                    </div>
                                </div>
                            </div>
                        </form>          
                        
                    </div>
                </div>
            </div>
         </div>
    </div>
</section>
<!-- Checkout Section End -->

@endsection
