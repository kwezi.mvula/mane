@extends('layouts.master')
@section('content')
<section id="about" data-stellar-background-ratio="0.5">
    <div class="container">
         <div class="row">

              <div class="col-md-6 col-sm-12">
                   <div class="about-info">
                        <div class="section-title wow fadeInUp" data-wow-delay="0.2s">
                             <h2>Thank You</h2>
                        </div>

                        <div class="wow fadeInUp" data-wow-delay="0.4s">
                             <p>Thank you for shopping at Knotty Mane. You will recieve a summary of your order via email. If you have any questions about your order, feel free to <a href="{{ asset('/contact-us') }}">contact us</a>.</p>
                             <p>Talk to you soon.</p>
                             <p>Knotty Mane</p>
                        </div>
                   </div>
              </div>

              <div class="col-md-6 col-sm-12">
                   <div class="wow fadeInUp about-image" data-wow-delay="0.6s">
                        <img src="images/about-image.jpg" class="img-responsive" alt="">
                   </div>
                   
              </div>
              
         </div>
    </div>
</section>
@include('includes.popular')
@endsection