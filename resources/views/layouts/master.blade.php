<!DOCTYPE html>
<html lang="en">
<head>

     <title>{{ config('app.name', 'Knotty-Mane') }}</title>

     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=Edge">
     <meta name="description" content="">
     <meta name="keywords" content="">
     <meta name="author" content="">
     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

     <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css')}}">
     <link rel="stylesheet" href="{{ asset('/css/font-awesome.min.css')}}">
     <link rel="stylesheet" href="{{ asset('/css/animate.css')}}">
     <link rel="stylesheet" href="{{ asset('/css/owl.carousel.css')}}">
     <link rel="stylesheet" href="{{ asset('/css/owl.theme.default.min.css')}}">
     <link rel="stylesheet" href="{{ asset('/css/magnific-popup.css')}}">
     <link rel="stylesheet" href="{{ asset('/css/cart.css')}}">

     <!-- MAIN CSS -->
     <link rel="stylesheet" href="{{ asset('/css/templatemo-style.css')}}">

</head>
<body>

     <!-- PRE LOADER -->
     <section class="preloader">
          <div class="spinner">
               <span class="spinner-rotate"></span>
          </div>
     </section>

     <!-- MENU -->
     @include('includes.menu')
     <!-- MENU -->
     
     @yield('content')

     <!-- FOOTER -->
     @include('includes.footer')
     <!-- FOOTER -->



     <!-- SCRIPTS -->
     <script src="{{ asset('/js/jquery.js')}}"></script>
     <script src="{{ asset('/js/bootstrap.min.js')}}"></script>
     <script src="{{ asset('/js/jquery.stellar.min.js')}}"></script>
     <script src="{{ asset('/js/wow.min.js')}}"></script>
     <script src="{{ asset('/js/owl.carousel.min.js')}}"></script>
     <script src="{{ asset('/js/jquery.magnific-popup.min.js')}}"></script>
     <script src="{{ asset('/js/smoothscroll.js')}}"></script>
     <script src="{{ asset('/js/custom.js')}}"></script>

</body>
</html>
