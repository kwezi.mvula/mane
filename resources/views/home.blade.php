@extends('layouts.master')
@section('content')

<!-- HOME -->
     <section id="home" class="slider" data-stellar-background-ratio="0.5">
          <div class="row">

                    <div class="owl-carousel owl-theme">
                         <div class="item item-first">
                              <div class="caption">
                                   <div class="container">
                                        <div class="col-md-8 col-sm-12">
                                             <h3>Face Products</h3>
                                             <h1>Intended to be used on acne prone skin. To soothe and heal dry, irritated skin....</h1>
                                             <a href="{{asset('/products/face')}}" class="section-btn btn btn-default smoothScroll">Read More</a>
                                        </div>
                                   </div>
                              </div>
                         </div>

                         <div class="item item-second">
                              <div class="caption">
                                   <div class="container">
                                        <div class="col-md-8 col-sm-12">
                                             <h3>Body Products</h3>
                                             <h1>Our hand rescue cream provides intensive moisture and hydration to dry skin....</h1>
                                             <a href="{{asset('/products/body')}}" class="section-btn btn btn-default smoothScroll">Read More</a>
                                        </div>
                                   </div>
                              </div>
                         </div>

                         <div class="item item-third">
                              <div class="caption">
                                   <div class="container">
                                        <div class="col-md-8 col-sm-12">
                                             <h3>Hair Products</h3>
                                             <h1>Our luxurious Shea hair butter is whipped to a creamy consistency and melts upon contact....</h1>
                                             <a href="{{asset('/products/hair')}}" class="section-btn btn btn-default smoothScroll">Read More</a>
                                        </div>
                                   </div>
                              </div>
                         </div>

                         <div class="item item-fourth">
                            <div class="caption">
                                 <div class="container">
                                      <div class="col-md-8 col-sm-12">
                                           <h3>Ingredients we use</h3>
                                           <h2 style="color:white">All our products are handmade in small batches using sustainably sourced ingredients that are free from harsh chemicals...</h2>
                                        <a href="{{asset('/products/ingredients')}}" class="section-btn btn btn-default smoothScroll">Read More</a>
                                      </div>
                                 </div>
                            </div>
                       </div>
                    </div>
          </div>
     </section>

     <!-- ABOUT -->
     <section id="about" data-stellar-background-ratio="0.5">
          <div class="container">
               <div class="row">

                    <div class="col-md-6 col-sm-12">
                         <div class="about-info">
                              <div class="section-title wow fadeInUp" data-wow-delay="0.2s">
                                   <h4>Read our story</h4>
                                   <h2>A little about us</h2>
                              </div>

                              <div class="wow fadeInUp" data-wow-delay="0.4s">
                                   <p>One thing that is so rewarding about starting a handmade skincare business is the positive impact the process has on the quality of our lives and those around us that benefit from it. It compelled me to be mindful and intentional about what I do, how I choose to live and what I consume for the health of my body and mind. I have found that it helps me cultivate discipline, diligence.....</p>
                              </div>
                              <a href="{{asset('/about-us')}}" class="section-btn btn btn-default smoothScroll">Read More</a>
                         </div>
                    </div>

                    <div class="col-md-6 col-sm-12">
                         <div class="wow fadeInUp about-image" data-wow-delay="0.6s">
                              <img src="images/about-image.jpg" class="img-responsive" alt="">
                         </div>
                         
                    </div>
                    
               </div>
          </div>
     </section>

     @include('includes.categories')

     @include('includes.recent')

     @include('includes.popular')

     @include('includes.latest')

@endsection
