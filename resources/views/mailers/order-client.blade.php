
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "https://www.w3.org/TR/html4/strict.dtd"><html lang="en"><head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
    <style type="text/css" nonce="9QC7kr1xmqTrejSiQzzrfQ">
            body,td,div,p,a,input {font-family: arial, sans-serif;}
    </style>
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Order Confirmation - KnottyMane</title>
    
    <style type="text/css" nonce="9QC7kr1xmqTrejSiQzzrfQ">
    body, td{
        font-size:13px
        } 
        a:link, a:active {color:#1155CC; text-decoration:none} a:hover {text-decoration:underline; cursor: pointer} a:visited{color:##6611CC} img{border:0px} pre { white-space: pre; white-space: -moz-pre-wrap; white-space: -o-pre-wrap; white-space: pre-wrap; word-wrap: break-word; max-width: 800px; overflow: auto;} 
        .logo { 
            left: -7px; position: relative; 
        }
    </style>
    
    <body>
        <div class="bodycontainer">
        <div class="maincontent">
            <table width=100% cellpadding=0 cellspacing=0 border=0 class="message">
                <tr>
                    <td align=right>
                        <tr>
                            <td colspan=2 style="padding-bottom: 4px;">
                                <tr>
                                    <td colspan=2>
                                        <table width=100% cellpadding=12 cellspacing=0 border=0>
                                            <tr>
                                                <td>
                                                    <div style="overflow: hidden;"><font size=-1>
                                                        <u></u>
    <div style="background-color:#ffffff;overflow-x:hidden;padding:0">
    <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="border:0px;border-collapse:collapse;margin:0 auto">
    <tbody>
        <tr>
    <td align="center" style="background-color:rgb(255,255,255)" bgcolor="#ededed">
    <table class="m_6618769179249232705container-main" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:100%">
    <tbody><tr class="m_6618769179249232705main-row">
    <td align="center" style="width:100%;background-color:#ffffff" bgcolor="#ffffff">
    <table width="600" align="center" class="m_6618769179249232705container" border="0" cellpadding="0" cellspacing="0" style="width:600px">
    <tbody><tr><td height="20"></td></tr>
    <tr>
    <th width="225" align="left" class="m_6618769179249232705container-wrap" valign="middle" style="vertical-align:middle">
    <table width="225" align="left" class="m_6618769179249232705container" border="0" cellpadding="0" cellspacing="0" style="width:140px">
    <tbody><tr>
    <td align="left" class="m_6618769179249232705icon-center">
    <img src="https://www.knottymane.com/images/logo.jpg" width="auto" height="auto" alt="" style="display:block;max-width:225px">
    </td>
    </tr>
    <tr><td class="m_6618769179249232705pt-20"></td></tr>
    </tbody></table>
    </th>
    <th width="320" align="center" class="m_6618769179249232705container-wrap" valign="middle" style="vertical-align:middle;width:320px">
    <table width="320" align="center" class="m_6618769179249232705container" border="0" cellpadding="0" cellspacing="0" style="width:320px">
    <tbody>
    <tr><td class="m_6618769179249232705pt-20"></td></tr>
    </tbody></table>
    </th>
    <th width="140" align="right" class="m_6618769179249232705container-wrap" valign="middle" style="vertical-align:middle">
    <table width="140" align="right" class="m_6618769179249232705container" border="0" cellpadding="0" cellspacing="0" style="width:140px">
    <tbody><tr></tr>
    </tbody></table>
    </th>
    </tr>
    <tr><td height="20"></td></tr>
    </tbody></table>
    </td>
    </tr>
    </tbody><td><div></div></td></table>
    </td>
    </tr>
    </tbody></table><table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="border:0px;border-collapse:collapse;margin:0 auto">
    <tbody><tr>
    <td align="center" style="background-color:rgb(255,255,255)" bgcolor="#ededed">
    <table class="m_6618769179249232705container-main" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:100%">
    <tbody><tr class="m_6618769179249232705main-row">
    <td align="center" style="width:100%;background-color:#ffffff" bgcolor="#ffffff">
    <table width="670" align="center" class="m_6618769179249232705container" border="0" cellpadding="0" cellspacing="0" style="width:670px">
    <tbody><tr><td align="center" class="m_6618769179249232705container-image">
    <img src="https://www.knottymane.com.co.za/images/logo.jpg" width="100%" height="auto" alt="KnottyMane" style="max-width:670px;display:block;border:0px"></td></tr>
    </tbody></table>
    </td>
    </tr>
    </tbody><td><div></div></td></table>
    </td>
    </tr>
    </tbody></table><table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="border:0px;border-collapse:collapse;margin:0 auto">
    <tbody><tr>
    <td align="center" style="background-color:rgb(255,255,255)" bgcolor="#ededed">
    <table class="m_6618769179249232705container-main" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:100%">
    <tbody><tr class="m_6618769179249232705main-row">
    <td align="center" style="width:100%;background-color:#ffffff" bgcolor="#ffffff">
    <table width="100%" class="m_6618769179249232705container" align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%">
    <tbody><tr><td height="45"></td></tr>
    </tbody></table>
    <table width="600" align="center" class="m_6618769179249232705container" border="0" cellpadding="0" cellspacing="0" style="width:600px">
    <tbody><tr>
    <td align="left" style="padding-left:20px;border-left:4px solid #0EEAFF">
    <table width="580" align="left" class="m_6618769179249232705container" border="0" cellpadding="0" cellspacing="0" style="width:580px">
    <tbody><tr>
    <td height="4"></td>
    </tr>
    <tr>
    <td style="text-align:left;font-family:&#39;Montserrat&#39;,Arial,Helvetica,sans-serif;font-size:18px;line-height:20px;text-decoration:none;color:#000000;font-weight:400;text-transform:uppercase;letter-spacing:0.05em">
    Order confirmation &amp; PROFORMA INVOICE</td>
    </tr>
    <tr>
    <td height="10"></td>
    </tr>
    <tr>
    <td style="text-align:left;font-family:&#39;Montserrat&#39;,Arial,Helvetica,sans-serif;font-size:13px;line-height:13px;text-decoration:none;color:#bdbdbd;font-weight:400;text-transform:uppercase;letter-spacing:0.05em">
    Do not reply to this email.
    </td>
    </tr>
    <tr>
    <td style="padding-top:5px;padding-bottom:20px;text-align:left;font-family:&#39;Open Sans&#39;,Arial,Helvetica,sans-serif;font-size:13px;line-height:20px;text-decoration:none;color:#444444;font-weight:400"><strong>Order Number: {{$Orders->order_code}}</strong></td>
    </tr></tbody></table>
    <u></u>
    <u></u><table>
    <tbody><tr>
    </tr>
    </tbody></table>
    </td>
    </tr>
    </tbody></table>
    <table width="100%" class="m_6618769179249232705container" align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%">
    <tbody><tr><td height="20"></td></tr>
    </tbody></table>
    </td>
    </tr>
    </tbody><td><div></div></td></table>
    </td>
    </tr>
    </tbody></table><table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="border:0px;border-collapse:collapse;margin:0 auto">
    <tbody><tr>
    <td align="center" style="background-color:rgb(255,255,255)" bgcolor="#ededed">
    <table class="m_6618769179249232705container-main" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:100%">
    <tbody><tr class="m_6618769179249232705main-row">
    <td align="center" style="width:100%;background-color:#ffffff" bgcolor="#ffffff">
    <table width="100%" class="m_6618769179249232705container" align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%">
    <tbody><tr><td height="5"></td></tr>
    </tbody></table>
    <table width="600" align="center" class="m_6618769179249232705container" border="0" cellpadding="0" cellspacing="0" style="width:600px">
    <tbody><tr>
    <td align="left" style="padding-left:20px;border-left:4px solid #0EEAFF">
    <table width="580" align="left" class="m_6618769179249232705container" border="0" cellpadding="0" cellspacing="0" style="width:580px">
    <tbody><tr>
    <td style="padding-top:5px;text-align:left;font-family:&#39;Open Sans&#39;,Arial,Helvetica,sans-serif;font-size:13px;line-height:20px;text-decoration:none;color:#444444;font-weight:400">
    {{$Orders->name}}<br>
    Your Order details are as follows:
    </td>
    </tr>
    <tr>
    <td height="10"></td>
    </tr>
    </tbody></table>
    </td>
    </tr>
    </tbody></table>
    <table width="100%" class="m_6618769179249232705container" align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%">
    <tbody><tr><td height="20"></td></tr>
    </tbody></table>
    </td>
    </tr>
    </tbody><td><div></div></td></table>
    </td>
    </tr>
    </tbody></table><table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="border:0px;border-collapse:collapse;margin:0 auto">
    <tbody><tr>
    <td align="center" style="background-color:rgb(255,255,255)" bgcolor="#ededed">
    <table class="m_6618769179249232705container-main" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:100%">
    <tbody><tr class="m_6618769179249232705main-row">
    <td align="center" style="width:100%;background-color:#ffffff" bgcolor="#ffffff">
    <table width="600" align="center" class="m_6618769179249232705container" border="0" cellpadding="0" cellspacing="0" style="width:600px">
    <tbody><tr>
    <td height="20"></td>
    </tr>
    <tr>
    <th class="m_6618769179249232705container-wrap" style="vertical-align:top" align="left" width="280">
    <table class="m_6618769179249232705container" align="left" border="0" cellpadding="0" cellspacing="0" width="280">
    <tbody><tr>
    <td style="text-decoration:none;font-family:&#39;Montserrat&#39;,Arial,Helvetica,sans-serif;font-size:14px;font-weight:400;line-height:21px;color:#000000;text-align:left;letter-spacing:0.075em;text-transform:uppercase">
    <strong>Your Information</strong></td>
    </tr>
    <tr>
    <td height="8"></td>
    </tr>
    <tr>
    <td style="text-decoration:none;font-family:&#39;Open Sans&#39;,Arial,Helvetica,sans-serif;font-size:13px;font-weight:400;line-height:20px;color:#444444;text-align:left">
    {{$Orders->name}}
    <br>Email : <a href="{{$Orders->email}}" style="color:#00305A" target="_blank">{{$Orders->email}}</a> 
    <br>Tel : <a href="tel:{{$Orders->phone}}" style="color:#00305A" target="_blank">{{$Orders->phone}}</a>

    <br><br>{{$Orders->address}}
    <br>{{$Orders->city}} 
    <br>{{$Orders->country}} 
    <br>{{$Orders->area_code}} 

    </td>
    </tr>
    <tr>
    <td height="20"></td>
    </tr>
    </tbody></table>
    </th>
    <th class="m_6618769179249232705container-wrap" style="vertical-align:top" align="right" width="280">
    <table>
    <tbody>
        <tr>
            <td style="text-decoration:none;font-family:&#39;Montserrat&#39;,Arial,Helvetica,sans-serif;font-size:14px;font-weight:400;line-height:21px;color:#000000;text-align:left;letter-spacing:0.075em;width:277px"><strong>PRODUCT DETAILS</strong><br>
    <span style="text-decoration:none;font-family:&#39;Open Sans&#39;,Arial,Helvetica,sans-serif;font-size:13px;font-weight:400;line-height:20px;color:#444444;text-align:left"><br></span>
    </td>
    </tr>
    </tbody></table>
    <table class="m_6618769179249232705container" align="right" border="0" cellpadding="0" cellspacing="0" width="250">
    <tbody><tr>
    <td>
     
    </td>
    </tr>
    <tr><td height="1"></td></tr>
    <tr>
    <th class="m_6618769179249232705container-wrap" align="left" valign="top" width="125">
    <table class="m_6618769179249232705container" align="left" border="0" cellpadding="0" cellspacing="0" width="250">
    <tbody>
            @foreach(Cart::content() as $item)
        <tr>
    <td class="m_6618769179249232705txt-left" style="text-decoration:none;font-family:&#39;Montserrat&#39;,Arial,Helvetica,sans-serif;font-size:13px;font-weight:400;line-height:14px;color:#444444;text-align:left;letter-spacing:0.075em">
    <br><b>Product Name:</b> {{$item->model->name}}<br>
    <b>Product Code:</b> {{$item->model->code}}<br>
    <b>QTY:</b> {{$item->qty}}
    </td>
    </tr>
   
    <tr><td height="5"></td></tr>
    <tr>
    <td align="center">
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody><tr>
    <td style="background:#eaeaea" align="center" height="2" bgcolor="#eaeaea" valign="middle">
    <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody><tr>
    <td style="background:#000000" align="center" height="2" bgcolor="#000000" valign="middle"></td>
    </tr>
    </tbody></table>
    </td>
    </tr>
    </tbody></table>
    </td>
    </tr>
    @endforeach
    <tr><td height="16"></td></tr>
    {{-- <tr>
    <td class="m_6618769179249232705txt-left" style="text-decoration:none;font-family:&#39;Montserrat&#39;,Arial,Helvetica,sans-serif;font-size:13px;font-weight:400;line-height:14px;color:#444444;text-align:left;letter-spacing:0.075em">
    <b>Product Name:</b> CHARLIE’s Sweet Dream <br>
    <b>QTY:</b> 2
    </td>
    </tr> --}}
    <tr><td height="5"></td></tr>
    <tr>
    <td align="center">
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody><tr>
    <td style="background:#eaeaea" align="center" height="2" bgcolor="#eaeaea" valign="middle">
    <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody><tr>
    <td style="background:#000000" align="center" height="2" bgcolor="#000000" valign="middle"></td>
    </tr>
    </tbody></table>
    </td>
    </tr>
    </tbody></table>
    </td>
    </tr>
    <tr><td height="16"></td></tr>
    <tr>
    <td class="m_6618769179249232705txt-left" style="text-decoration:none;font-family:&#39;Montserrat&#39;,Arial,Helvetica,sans-serif;font-size:13px;font-weight:400;line-height:14px;color:#444444;text-align:left;letter-spacing:0.075em">
    Subtotal: <b>ZAR {{Cart::subtotal()}}</b><br>
    Tax: <b>ZAR {{Cart::tax()}}</b><br>
    Total: <b>ZAR {{Cart::total()}}</b><br>
    </td>
    </tr>
    <tr><td height="5"></td></tr>
    <tr>
    <td align="center">
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody><tr>
    <td style="background:#eaeaea" align="center" height="2" bgcolor="#eaeaea" valign="middle">
    <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody><tr>
    <td style="background:#000000" align="center" height="2" bgcolor="#000000" valign="middle"></td>
    </tr>
    </tbody></table>
    </td>
    </tr>
    </tbody></table>
    </td>
    </tr>
    <tr><td height="16"></td></tr>
    </tbody></table>
    </th>
    <th class="m_6618769179249232705container-wrap" align="right" valign="top" width="125">
    </td></tr>
    <tr>
    <td height="25"></td>
    </tr>
    </tbody></table>
    </th>
    </tr>
    </tbody></table>
    </th></tr>
    </tbody></table>
    </td>
    </tr>
    </tbody><td><div></div></td></table></td></tr></tbody></table><table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="border:0px;border-collapse:collapse;margin:0 auto">
    <tbody><tr>
    <td align="center" style="background-color:rgb(255,255,255)" bgcolor="#ededed">
    <table class="m_6618769179249232705container-main" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:100%">
    <tbody><tr class="m_6618769179249232705main-row">
    <td align="center" style="width:100%;background-color:#ffffff" bgcolor="#ffffff">
    <table width="600" align="center" class="m_6618769179249232705container" border="0" cellpadding="0" cellspacing="0" style="width:600px">
    <tbody><tr>
    <td height="5"></td>
    </tr>
    <tr>
    <th width="280" align="left" class="m_6618769179249232705container-wrap" valign="top" style="vertical-align:top;width:280px">
    <table width="280" align="left" class="m_6618769179249232705container" border="0" cellpadding="0" cellspacing="0" style="width:280px">
    <tbody>
    <tr>
    <td height="40"></td>
    </tr>
    </tbody></table>
    </th>
    <th width="280" align="right" class="m_6618769179249232705container-wrap" valign="top" style="vertical-align:top">
    <table width="280" align="right" class="m_6618769179249232705container" border="0" cellpadding="0" cellspacing="0" style="width:280px">
    <tbody>
    <tr>
    <td height="40"></td>
    </tr>
    </tbody></table>
    </th>
    </tr>
    <tr>
    <td height="1"></td>
    </tr>
    </tbody></table>
    </td>
    </tr>
    </tbody><td><div></div></td></table>
    </td>
    </tr>
    </tbody></table><table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="border:0px;border-collapse:collapse;margin:0 auto">
    <tbody><tr>
    <td align="center" style="background-color:rgb(255,255,255)" bgcolor="#ededed">
    <table class="m_6618769179249232705container-main" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:100%">
    <tbody><tr class="m_6618769179249232705main-row">
    <td align="center" style="width:100%;background-color:#ffffff" bgcolor="#ffffff">
    <table width="670" align="center" class="m_6618769179249232705container" border="0" cellpadding="0" cellspacing="0" style="width:670px;background-color:#000;background:#000;text-align:center">
    <tbody><tr>
    <td height="20"></td>
    </tr>
    <tr>
    <td style="text-align:center;font-family:&#39;Montserrat&#39;,Arial,Helvetica,sans-serif;font-size:18px;line-height:20px;text-decoration:none;color:#ffffff;font-weight:400;text-transform:uppercase;letter-spacing:0.05em">
    Thank You For Your Order</td>
    </tr>
    <tr>
    <td height="20"></td>
    </tr>
    </tbody></table>
    </td>
    </tr>
    </tbody><td><div></div></td></table>
    </td>
    </tr>
    </tbody></table><table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="border:0px;border-collapse:collapse;margin:0px auto;opacity:1">
    <tbody><tr>
    <td align="center" style="background-color:rgb(255,255,255)" bgcolor="#ededed">
    <table class="m_6618769179249232705container-main" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:100%">
    <tbody><tr class="m_6618769179249232705main-row">
    <td align="center" style="width:100%;background-color:#ffffff" bgcolor="#ffffff">
    <table width="670" align="center" class="m_6618769179249232705container" border="0" cellpadding="0" cellspacing="0" style="width:670px">
    <tbody><tr><td align="center" class="m_6618769179249232705container-image"></td></tr>
    </tbody></table>
    </td>
    </tr>
    </tbody><td><div></div></td></table>
    </td>
    </tr>
    </tbody></table><table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="border:0px;border-collapse:collapse;margin:0px auto;opacity:1">
    <tbody><tr>
    <td align="center" style="background-color:rgb(255,255,255)" bgcolor="#ededed">
    <table class="m_6618769179249232705container-main" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:100%">
    <tbody><tr class="m_6618769179249232705main-row">
    <td align="center" style="width:100%;background-color:#ffffff" bgcolor="#ffffff">
    <table width="670" align="center" class="m_6618769179249232705container" border="0" cellpadding="0" cellspacing="0" style="width:670px">
    <!-- <tbody><tr><td align="center" class="m_6618769179249232705container-image"><img src="http://www.vapedistroint.co.za/images/warning.png" width="100%" height="auto" alt="VDI" style="max-width:670px;display:block;border:0px"></td></tr>
    </tbody> -->
    </table>
    </td>
    </tr>
    </tbody><td><div></div></td></table>
    </td>
    </tr>
    </tbody></table><table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="border:0px;border-collapse:collapse;margin:0 auto">
    <tbody><tr>
    <td align="center" style="background-color:rgb(255,255,255)" bgcolor="#ffffff">
    <table class="m_6618769179249232705container-main" align="center" border="0" cellpadding="0" cellspacing="0" style="min-width:100%">
    <tbody><tr class="m_6618769179249232705main-row">
    <td align="center" style="width:100%;background-color:#ffffff" bgcolor="#ffffff">
    <table width="670" align="center" class="m_6618769179249232705container" border="0" cellpadding="0" cellspacing="0" style="width:670px">
    <tbody><tr>
    <td height="35"></td>
    </tr>
    <tr>
    <th width="280" align="left" class="m_6618769179249232705container-wrap" valign="top" style="vertical-align:top;width:280px">
    <table width="280" align="left" class="m_6618769179249232705container" border="0" cellpadding="0" cellspacing="0" style="width:280px">
    <tbody><tr>
    <td class="m_6618769179249232705txt-left" style="vertical-align:top;text-align:left;font-family:&#39;Open Sans&#39;,Arial,Helvetica,sans-serif;font-size:13px;line-height:20px;text-decoration:none;color:#444444;font-weight:400">Copyright © KnottyMane
    <!--<br>Contact KnottyMane: <br> Tel: <a href="tel: 010 006 0632" style="color:#00305A" target="_blank"> 010 006 0632</a>-->
    <br> Email: <a href="mailto:info@knottymane.com" style="color:#00305A" target="_blank">info@knottymane.com</a> </td>
    </tr>
    <tr>
    <td height="13"></td>
    </tr>
    <tr><td style="vertical-align:top;text-align:left;font-family:&#39;Open Sans&#39;,Arial,Helvetica,sans-serif;font-size:13px;line-height:13px;text-decoration:none;color:#444444;font-weight:400"><br></td></tr>
    <tr>
    <td height="39"></td>
    </tr>
    </tbody></table>
    </th>
    <th width="280" align="right" class="m_6618769179249232705container-wrap" valign="top" style="vertical-align:top">
    <table width="280" align="right" class="m_6618769179249232705container" border="0" cellpadding="0" cellspacing="0" style="width:280px">
    <tbody><tr class="m_6618769179249232705disable">
    <td height="5"></td>
    </tr>
    <tr>
    <td valign="top" align="right" class="m_6618769179249232705fl-left">
    <table border="0" cellpadding="0" cellspacing="0" align="right">
    <tbody><tr>
    <!-- <td align="center" valign="middle" style="padding-right:6px"><a href="https://twitter.com/CityLodgeHotels" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://twitter.com/CityLodgeHotels&amp;source=gmail&amp;ust=1572410410966000&amp;usg=AFQjCNFAdYZ8D2RGSh_RcijN1P36hBcrKA"><img src="https://ci5.googleusercontent.com/proxy/AQNpOg0NV0EQhCuOo8zJ3yRr4LPEEiS0SmVyyhQnSL7UxAhu8yAR797lmCEtndZagzhHhCfIOU7VkW7MpfjLKyhsJfqPpd9SvEz3qrnzQagJ9tJcazhl9unTZiUZeBRqaScW6QujPanJa-CMQLEfoK5XKWEnP7yTYR5yLk5F6OqQo0Dyp7_iiG01dgSnwls4Jrw0c1LxeMCts-j-IlzO1Jhf0kBMWxqqFXT5DTFuI_DqPLscBF6OK8t7mW0nK-3XrA=s0-d-e1-ft#http://www.stampready.net/dashboard/editor-3-5/uploads/image_uploads/edited/2019/05/16/FnVBCag3y27j4ceLKD8srW6HhzipRmfE9kXxbTYUAG.png?random=LOKJvu6C6lUi2GGrJNkdHuP97g0fBUpk" width="20" height="20" alt="" style="display:block;border:0px;max-width:20px"></a></td> -->
    <td align="center" valign="middle" style="padding:0 6px"><a href="https://www.facebook.com/knottymane/" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://www.facebook.com/City.lodge.hotels.limited&amp;source=gmail&amp;ust=1572410410966000&amp;usg=AFQjCNEt5EafGsVwlEjGoq-SeWYWO_edNA"><img src="https://ci3.googleusercontent.com/proxy/5ISNrIqg8GilFHn8M3p2GvMVfoe_8W8MvP5f-7x78h8G3Kvqi7Q8H5Deuy1LDABU1jtQiVTNplKiDy1Xkpj8Vcuj7YiDTLw4qVXqv-5AOMq3Cqa7dLPCsXhtrUBjHcVFFySn8AH8zc5LjglYYy1vfNVmo4gfS7XhdXfQ3dOUnc_tz97cl378u-AvTUaihBb6XuIezzCftifSteIRkXPfG4zeBnlsjAvarTwFBRe-2KZPiCUVU5yCGAaRhTdNe5krxw=s0-d-e1-ft#http://www.stampready.net/dashboard/editor-3-5/uploads/image_uploads/edited/2019/05/16/vpB9f8lGJ6cmVCDjKXhqsPzo2OgUFdkei4NLbnM7ZI.png?random=zGh8SowOYGuU02ZURPHUOI19QuqHKL18" width="20" height="20" alt="" style="display:block;border:0px;max-width:20px"></a></td>
    </tr>
    </tbody></table>
    </td>
    </tr>
    <tr>
    <td height="39"></td>
    </tr>
    </tbody></table>
    </th>
    </tr>
    </tbody></table>
    </td>
    </tr>
    </tbody><td><div></div></td></table>
    </td>
    </tr>
    </tbody></table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
            <td height="35"> </td>
        </tr>
        <tr>
            <td height="35"> </td>
        </tr>
    </table>
    <img src="https://ci5.googleusercontent.com/proxy/YYIE_Cih4sy9mJSmbfVV5UCpW2uwRxH1IrfjTj9OtN9jMNjdR3lf4PFiHurAbaxAZSj1KTV0l7epfIRdBPGmfMXNbkreXvplPfZFjflX3slcEKc51zNhra0ph9iCwRetbGrdQ9-jdSMa3vIN0E585dB24e7HrFmTvgFQXScaDOcSeDM98EJ3Br-rqSvWvstoxGcT-GYcvJNpfxzEvi4qLAxIrabv_wWoyvf5e8aO8hkTxQA8BdV8YzV8JOLdnfGHiyQ3T24QrBWShi29FCNFByUQrIMzok2l-80lboW_qRA0NxR6aH26Yo-GuemHV8t69vCVBJdS81u4bPLIBADLR64PzhuabBz7e_lw4OTr1jI3XurxZpSXX7zQ-MxTzcLePbG5LTiNfK5sBtbxwu4p4ZppFx2g3QOY_Q=s0-d-e1-ft#https://u3605716.ct.sendgrid.net/wf/open?upn=rlhdxv42-2BeE9lFF577vNyoP1QSBAA9tMwGtpYu1gtc6B8zWCc3Rf-2B7uqcFED8cl9KiHpBwqJLZZqTd01t0Luc1TvX369qnrbG3ceDpYfIGtbX7r50Ha9O8C1Zfp9o4IifpinkLsfyF07-2B8PzakIaDlHuCKs-2BmTxm5h6cllRZWUOiQgEijS-2B6bwljaUlNhFvb7XQTsaj9OIXF0R2ujfPmtVOZIIiarCw51R7YQdKYZec-3D" alt="" width="1" height="1" border="0" style="height:1px!important;width:1px!important;border-width:0!important;margin-top:0!important;margin-bottom:0!important;margin-right:0!important;margin-left:0!important;padding-top:0!important;padding-bottom:0!important;padding-right:0!important;padding-left:0!important">
    </div>
    </font>
    </div>
    </table>
    </table>
    </div>
    </div>
    </body>