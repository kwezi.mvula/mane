<section id="team" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
                    <h2>Recent Products</h2>
                </div>
            </div>

            @forelse($recent as $product)
            <div class="col-md-3 col-sm-4">
                <!-- POPULAR THUMB -->
                <div class="menu-thumb">
                    <a href="/storage/{{$product->image}}" class="image-popup" title="Popular Products">
                        <img src="/storage/{{$product->image}}" class="img-responsive" alt="">

                        <div class="menu-info">
                            <div class="menu-item">
                                <h3><span style="color: whitesmoke">{{ $product->pro_name }}</span>
                                <ul class="social-icon">
                                    <li><a style="font-size: 25px" href="{{ URL::to('cart/addItem/'.$product->id) }}" class="fa fa-shopping-cart"></a></li>
                                    <li><a style="font-size: 25px" href="{{ URL::to('details/'.$product->id) }}" class="fa fa-info-circle"></a></li>
                                </ul>
                                </h3>
                            </div>
                            <div class="menu-price">
                                <span>${{ $product->pro_price }}</span>
                            </div>              
                        </div>
                    </a>
                </div>
            </div>  
            @empty
            <div class="col-md-4 col-sm-6">
                <div class="team-info">
                  <h2>Products currently unavailable</h2>
                </div>
            </div>  
            @endforelse
            
        </div>
    </div>
</section>