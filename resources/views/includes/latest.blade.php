<section id="team" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
                    <h2>Latest Articles</h2>
                    <h4>Blog Posts</h4>
                </div>
            </div>
            @foreach($articles as $article)
                <div class="col-md-4 col-sm-4">
                    <div class="team-thumb wow fadeInUp" data-wow-delay="0.2s">
                        <img src="/storage/{{$article->image}}" class="img-responsive" alt="">
                        <div class="team-hover">
                            <div class="team-item">
                                <h4>{{ $article->excerpt }}</h4> 
                                <ul class="social-icon">
                                    <li><a style="font-size: 25px" href="{{ URL::to('post/'.$article->slug) }}" class="fa fa-info-circle"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="team-info">
                        <h3>{{ $article->title }}</h3>
                        <p>{{ $article->created_at }}</p>
                    </div>
                </div>   
            @endforeach        
        </div>
    </div>
</section>