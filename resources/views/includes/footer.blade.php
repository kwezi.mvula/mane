<!-- FOOTER -->
<footer id="footer" data-stellar-background-ratio="0.5">
    <div class="container">
         <div class="row">

              <div class="col-md-3 col-sm-8">
                   <div class="footer-info">

                   </div>
              </div>

              <div class="col-md-3 col-sm-8">
                   <div class="footer-info">
                        <div class="section-title">
                             <h2 class="wow fadeInUp" data-wow-delay="0.2s">Contact Us</h2>
                        </div>
                        <address class="wow fadeInUp" data-wow-delay="0.4s">
                             <p>+353 89 9865921</p>
                             <p><a href="mailto:info@knotty-mane.com">info@knottymane.com</a></p>
                        </address>
                   </div>
              </div>

              <div class="col-md-4 col-sm-8">
                   <div class="footer-info footer-open-hour">
                        <div class="section-title">
                             <h2 class="wow fadeInUp" data-wow-delay="0.2s">Follow Us</h2>
                        </div>
                        <ul class="wow fadeInUp social-icon" data-wow-delay="0.4s">
                          <li><a href="#" class="fa fa-facebook-square" attr="facebook icon"></a></li>
                          <li><a href="#" class="fa fa-twitter"></a></li>
                          <li><a href="#" class="fa fa-instagram"></a></li>
                          <li><a href="#" class="fa fa-google"></a></li>
                     </ul>

                     <div class="wow fadeInUp copyright-text" data-wow-delay="0.8s">
                          <p><br>Copyright &copy; 2020 <br>Knotty Mane
                          <br>Developed by: <a href="#">reenDev</a></p>
                     </div>
                   </div>
              </div>

              <div class="col-md-2 col-sm-4">

              </div>

         </div>
    </div>
</footer>