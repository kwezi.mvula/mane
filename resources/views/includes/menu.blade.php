<!-- MENU -->
<section class="navbar custom-navbar navbar-fixed-top" role="navigation">
    <div class="container">

         <div class="navbar-header">
              <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                   <span class="icon icon-bar"></span>
                   <span class="icon icon-bar"></span>
                   <span class="icon icon-bar"></span>
              </button>

              <!-- lOGO TEXT HERE -->
              <a href="{{ asset('/')}}" class="navbar-brand"><img style="width:65px;height:45px" src="{{URL::asset('/images/logo.JPG')}}"></a>
              <a href="{{ asset('/')}}" class="navbar-brand">Knotty <span>-</span> Mane</a>
         </div>

         <!-- MENU LINKS -->
         <div class="collapse navbar-collapse">
              <ul class="nav navbar-nav navbar-nav-first">
                   <li><a href="{{ asset('/')}}" class="smoothScroll">Home</a></li>
                   <li><a href="#" data-toggle="dropdown"><span data-hover="ShortCodes">Products</span><span class="caret"></span></a>
                         <ul class="dropdown-menu">
                                   <li><a href="{{ asset('/face-products')  }}" class="smoothScroll" style="color: #797876">Face</a></li>
                                   <li><a href="{{ asset('/body-products')  }}" class="smoothScroll" style="color: #797876">Body</a></li>
                                   <li><a href="{{ asset('/hair-products')  }}" class="smoothScroll" style="color: #797876">Hair</a></li>
                                   <li><a href="{{ asset('/ingredients')  }}" class="smoothScroll" style="color: #797876">Ingredients</a></li>
                         </ul>
                   </li>
                   <li><a href="{{ asset('/shop') }}" class="smoothScroll">Shop</a></li>
                   <li><a href="{{ asset('/about-us') }}" class="smoothScroll">About Us</a></li>
                   <li><a href="{{ asset('/contact-us') }}" class="smoothScroll">Contact Us</a></li>
              </ul>
              @guest
              <ul class="nav navbar-nav navbar-right">
               <a href="{{ asset('/login') }}" class="section-btn">Login</a>        
              </ul>
              @else
              <ul class="nav navbar-nav navbar-right">
                    <li><a href="{{ asset('/cart') }}" class="fa fa-shopping-cart">({{Cart::count()}})</a></li>            
                    <li><a href="#" data-toggle="dropdown"><span data-hover="ShortCodes">{{ Auth::user()->name }}</span><span class="caret"></span></a>
                         <ul class="dropdown-menu">
                              @if(auth()->user()->role_id == 1)
                                   <li><a href="{{ asset('/admin')  }}" style="color: #797876">Dashboard</a></li>
                              @else
                                   <li><a href="{{ asset('/')  }}" style="color: #797876">Normal User</a></li>
                              @endif
                              <li>
                                   <a href="{{ route('logout') }}" style="color: #797876"
                                        onclick="event.preventDefault();
                                                  document.getElementById('logout-form').submit();">
                                             {{ __('Logout') }}
                                   </a>
                                   <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                   </form>
                              </li>
                         </ul>
                    </li>
                    <a href="{{ asset('/blog') }}" class="section-btn">Blog</a>
              </ul>
              @endguest
         </div>

    </div>
</section>