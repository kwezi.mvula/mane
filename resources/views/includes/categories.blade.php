 <section id="team" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                 <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
                      <h2>Shop by Categories</h2>
                 </div>
            </div>
            <div class="accordian"> 
              <ul>
                @forelse ($categories as $category)
                  <li>
                    <div class="image_title">
                      <a href="/shop/{{ $category->slug }}">{{ $category->name }}</a>
                    </div>
                    <a href="/shop/{{ $category->slug }}">
                        <img style="width: 350px; height:350px" src="/storage/{{$category->image}}">
                    </a>
                  </li> 
                @empty  
                @endforelse 
                <li>
                  <div class="image_title">
                    <a href=""></a>
                  </div>
                  <a href="">
                      <img" src="">
                  </a>
                </li>
              </ul>  
            </div>
        </div>
    </div>
</section>