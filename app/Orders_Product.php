<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders_Product extends Model
{
   //Table Name
   protected $table = 'orders_products';
   //fillable
   protected $fillable = ['order_id','item_name','quantity','order_code'];
   //Timestamps
   public $timestamps = true;

   public function orders(): BelongsTo
    {
        return $this->belongsTo(Orders::class);
    }
}
