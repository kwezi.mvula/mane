<?php

namespace App\Mail;

use App\Orders;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderClient extends Mailable
{
    use Queueable, SerializesModels;

    public $Orders;

    /**
     * Create a new message instance.
     *
     * @return void
     */ 
    public function __construct(Orders $Orders)
    {
        $this->Orders = $Orders;
    }
    

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to($this->Orders->email, $this->Orders->name)
        ->subject('Order Confirmation - Knotty Mane')
        ->view('mailers.order-client');
    }
}
