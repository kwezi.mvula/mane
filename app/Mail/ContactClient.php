<?php

namespace App\Mail;

use App\Contact;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactClient extends Mailable
{
    use Queueable, SerializesModels;

    public $Contact;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Contact $Contact)
    {
        $this->Contact =$Contact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to($this->Contact->email, $this->Contact->name)
                    ->subject('Thank You For Contacting Knotty Mane')
                    ->view('mailers.contact-client');
    }
}
