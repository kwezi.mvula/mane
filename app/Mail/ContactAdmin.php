<?php

namespace App\Mail;

use App\Contact;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactAdmin extends Mailable
{
    use Queueable, SerializesModels;

    public $Contact;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Contact $Contact)
    {
        $this->Contact = $Contact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to('info@knottymane.com', 'Knotty Mane')
        ->subject('New Contact Enquiry from Knotty Mane')
        ->view('mailers.contact-admin');
    }
}
