<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $table = 'products';
    public $fillable = ['pro_name','pro_code','pro_price','pro_info','pro_cat','image'];
}
