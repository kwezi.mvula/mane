<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    public $fillable = ['order_id'];
    public function orders_products(): HasMany
    {
        return $this->hasMany(Orders_Products::class);
    }

  
}
