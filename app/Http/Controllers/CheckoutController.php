<?php

namespace App\Http\Controllers;

use App\Orders;
use App\Product;
use App\Orders_Product;
use App\Mail\OrderAdmin;
use App\Mail\OrderClient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Billow\Contracts\PaymentProcessor;
use Gloudemans\Shoppingcart\Facades\Cart;

class CheckoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $randomOrderNumber = rand();
        $Items = Cart::content();
        return view('checkout.index')->with([
            'Items' => $Items,
            'randomOrderNumber' => $randomOrderNumber,
            ]);
    }

    public function cancel()
    {
        $popular = Product::inRandomOrder()
        ->take(4)
        ->get();
    
        return view('checkout.cancel')->with([
            'popular' => $popular,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, PaymentProcessor $payfast)
    {
        $Orders = new Orders;
        $Orders_Product = new Orders_Product;
        $randomOrderNumber = $request->input('order_code');

        //insert into orders table
        $Orders ->name = $request->input('name');
        $Orders ->surname = $request->input('surname');
        $Orders ->email = $request->input('email');
        $Orders ->phone = $request->input('phone');
        $Orders ->address = $request->input('address');
        $Orders ->city = $request->input('city');
        $Orders ->country = $request->input('country');
        $Orders ->area_code = $request->input('area_code');
        $Orders ->order_code = $randomOrderNumber;
        
        $Orders->save();

        $cartTotal = $request->input('total');

        //insert into orderproduct table
        foreach(Cart::content() as $item){
            Orders_Product::create([
            'order_id' => $Orders->id,
            'item_name' =>$item->name,
            'quantity' => $item->qty,
            'amount' => $cartTotal,
            ]);
        }

        $name = $request->input('name');
        $surname = $request->input('surname');
        $email = $request->input('email');

        // Build up payment Paramaters.
        $payfast->setBuyer($name, $surname, $email);
        $payfast->setAmount($cartTotal);
        foreach(Cart::content() as $item){
            $payfast->setItem('Knotty Mane', 'Hair and/or skin product');
        } 
        $payfast->setMerchantReference( $randomOrderNumber);

           // Send email to Client
           Mail::send(new OrderClient($Orders));
           // Send email to Admin
           Mail::send(new OrderAdmin($Orders));

        // Return the payment form.
        return $payfast->paymentForm('Confirm and Pay');
    }

    public function itn(Request $request, PaymentProcessor $payfast)
    {
        // Retrieve the Order from persistance. Eloquent Example.
        $order = Orders_Product::where('order_code', $request->get('order_code'))->firstOrFail(); // Eloquent Example

        // Verify the payment status.
        $status = $payfast->verify($request, $order->amount, $order->order_code)->status();

        // Handle the result of the transaction.
        switch( $status )
        {
            case 'COMPLETE': // Things went as planned, update your order status and notify the customer/admins.
                break;
            case 'FAILED': // We've got problems, notify admin and contact Payfast Support.
                break;
            case 'PENDING': // We've got problems, notify admin and contact Payfast Support.
                break;
            default: // We've got problems, notify admin to check logs.
                break;
        }
    }

    public function success()
    {
        $popular = Product::inRandomOrder()
        ->take(4)
        ->get();

        Cart::destroy();
        
        return view('checkout.thank_you')->with([
            'popular' => $popular,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
