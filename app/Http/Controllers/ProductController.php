<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;
use App\Category;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ingredients(){
        return view('front.ingredients');
    }

    public function body(){
        return view('front.body');
    }

    public function face(){
        return view('front.face');
    }

    public function hair(){
        return view('front.hair');
    }

    public function shop()
    {
        $categories = Category::get();
        $products = Product::all();
        return view('front.shop')->with([
            'categories' => $categories,
            'products' => $products,
        ]);
    }  

    public function ShopCat($slug)
    {
        $categories = Category::get();
        $products = Product::where('cat_id' , $slug)
        ->get();
        $popular = Product::inRandomOrder()
        ->take(4)
        ->get();
        $recent = Product::latest()
        ->take(4)
        ->get();
        $category = Category::where('slug',$slug)
        ->first();
        return view('front.Shopcat')->with([
            'recent' => $recent,
            'popular' => $popular,
            'products' => $products,
            'category' => $category,
            'categories' => $categories,
        ]);
    }  
   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function details($id)
    {
        $product = Product::findOrFail($id);
        $popular = Product::inRandomOrder()
        ->take(4)
        ->get();
        $recent = Product::latest()
        ->take(4)
        ->get();
        return view('front.details')->with([
            'recent' => $recent,
            'product' => $product,
            'popular' => $popular,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
