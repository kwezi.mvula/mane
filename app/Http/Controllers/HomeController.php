<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Product;
use App\Category;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $categories = Category::all();
        $products = Product::inRandomOrder()
        ->take(8)
        ->get();
        $popular = Product::inRandomOrder()
        ->take(4)
        ->get();
        $recent = Product::latest()
        ->take(4)
        ->get();
        $articles = Blog::latest()
        ->take(6)
        ->get();
        return view('home')->with([
            'recent' => $recent,
            'popular' => $popular,
            'products' => $products,
            'articles' => $articles,
            'categories' => $categories,
        ]);
    }

    public function thank_you()
    {
        $popular = Product::inRandomOrder()
        ->take(4)
        ->get();
        $recent = Product::latest()
        ->take(4)
        ->get();
        return view('front.thank_you')->with([
            'recent' => $recent,
            'popular' => $popular,
        ]);
    }
}
