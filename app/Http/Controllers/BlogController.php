<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Product;
use App\Category;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = Blog::latest()
        ->first();
        $popular = Product::inRandomOrder()
        ->take(4)
        ->get();
        $recent = Product::latest()
        ->take(4)
        ->get();
        $new = Blog::latest()
        ->take(2)
        ->get();

        $articles = Blog::latest()
        ->take(6)
        ->get();
        return view('front.blog')->with([
            'new' => $new,
            'post' => $post,
            'recent' => $recent,
            'popular' => $popular,
            'articles' => $articles,
        ]);
            
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function post($slug)
    {
        $post = Blog::where('slug',$slug)
        ->first();
        $articles = Blog::latest()
        ->take(6)
        ->get();
        return view('front.post')->with([
            'post' => $post,
            'articles' => $articles,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
